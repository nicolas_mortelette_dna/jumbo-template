import React from 'react';
import CustomScrollbars from 'util/CustomScrollbars';
import Navigation from "../../components/Navigation";

const SideBarContent = () => {
  const navigationMenus = [
    {
      name: 'sidebar.main',
      type: 'section',
      children: [
        {
          name: 'sidebar.dashboard',
          icon: 'view-dashboard',
          type: 'collapse',
          children: [
            {
              name: 'sidebar.dashboard.page1',
              type: 'item',
              link: '/app/sample-page'
            },
            {
              name: 'sidebar.dashboard.page2',
              type: 'item',
              link: '/app/sample-page2'
            }
          ]
        },
        {
          name: 'sidebar.components',
          icon: 'folder',
          type: 'collapse',
          children: [
            {
              name: 'sidebar.components.page1',
              type: 'item',
              link: '/app/sample-page3'
            },
            {
              name: 'sidebar.components.page2',
              type: 'item',
              link: '/app/sample-page4'
            },
            {
              name: 'sidebar.components.page3',
              type: 'item',
              link: '/app/sample-page5'
            }
          ]
        }
      ]
    },
    {
      name: 'Menu 2',
      type: 'section',
      children: [
        {
          name: 'Page 2.1',
          type: 'item',
          icon: 'email',
          link: '/app/mail'
        },
        {
          name: 'Page 2.2',
          type: 'item',
          icon: 'check-square',
          link: '/app/to-do'
        },
        {
          name: 'Page 2.3',
          type: 'item',
          icon: 'account-box',
          link: '/app/contact'
        }
      ]
    },
  ];

  return (
    <CustomScrollbars className=" scrollbar">
      <Navigation menuItems={navigationMenus}/>
    </CustomScrollbars>
  );
};

export default SideBarContent;
